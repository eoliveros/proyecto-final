package deloitte.academy.lesson01.entity;

import deloitte.academy.lesson01.run.Main;
import java.util.logging.Logger;

/**
 * Clase que hereda de la clase expendedora e implementa la interfaz CRUD.
 * 
 * @author Emiliano Oliveros
 * @since crud
 * {@link}https://www.tutorialspoint.com/java/java_documentation.htm
 * {@docRoot} /copyright.html">Copyright</a>.
 */
public class Articulo extends Expendedora implements Crud {
	private static final Logger LOGGER = Logger.getLogger(Articulo.class.getName());
	String nombre;

	public Articulo() {
		super();
	}

	/**
	 * Constructor que asigna el nombre
	 * 
	 * @param string codigo
	 * @param string articulo
	 * @param double precio
	 * @param int    cantidad
	 * @param string nombre
	 */
	public Articulo(String codigo, String articulo, double precio, int cantidad, String nombre) {
		super(codigo, articulo, precio, cantidad);
		this.nombre = nombre;

	}

	public Articulo(Articulo a) {
		super(a);
		this.nombre = a.nombre;

	}

	@Override
	/**
	 * Esta funcion registra un nuevo articulo. Valida tambien si no es repetido
	 * 
	 * @return mensaje "Articulo repetido"
	 * @retunr mensaje "Articulo registrado exitosamente".
	 */
	public void createProducto(Articulo articulo) {

		if (Main.listaArticulos2.contains(articulo)) {
			LOGGER.info("Articulo repetido");
		} else {
			Main.listaArticulos2.add(articulo);
			LOGGER.info("Articulo registrado exitosamente");
		}

	}

	/**
	 * Esta funcion actualiza un articulo en la lista y en el stock.
	 * 
	 * @return mensaje "producto actualizado exitosamente.
	 */
	@Override
	public void updateProducto(Articulo articulo, Articulo cambio) {
		Main.listaArticulos2.remove(articulo);
		Main.listaStock.remove(articulo);
		Main.listaArticulos2.add((Articulo) cambio);
		Main.listaStock.add(articulo);
		LOGGER.info("Producto actualizado exitosamente");

	}

	/**
	 * Esta funcion elimina un articulo de la lista y del stock.
	 * 
	 * @return mensaje "producto borrado exitosamente"
	 */
	@Override
	public void deleteProducto(Articulo articulo) {
		Main.listaArticulos2.remove(articulo);
		Main.listaStock.remove(articulo);
		LOGGER.info("Producto borrado exitosamente");

	}

	/**
	 * Esta funcion arroja los parametros del articulo
	 */
	@Override
	public void readProducto(Articulo articulo) {
		System.out.println(articulo.toString());

	}

	/**
	 * Esta funcion despliega los titulos de la lista.
	 * 
	 * @return status
	 */
	@Override
	public String toString() {
		String status = "Codigo: " + this.Codigo + "\nArticulo: " + this.Articulo + "\nPrecio: " + this.Precio
				+ "\nStock: " + this.Cantidad;
		return status;
	}

	/**
	 * Esta funcion simula una venta y la agrega a la lista de vendidos.
	 * 
	 * @exception IOException On input error.
	 * @see IOException
	 */
	@Override
	public void vender() {
		// TODO Auto-generated method stub
		try {
			if (Main.listaArticulos2.contains(this)) {
				Articulo aux = new Articulo(this);
				this.setCantidad(this.getCantidad() - 1);
				aux.setCantidad(1);
				Main.listaVendidos.add(aux);
			} else {
				LOGGER.info("Articulo Agotado!");
			}
		} catch (Exception e) {
			LOGGER.severe("Error " + e);

		}
	}
}
