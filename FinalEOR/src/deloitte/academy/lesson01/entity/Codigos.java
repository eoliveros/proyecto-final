package deloitte.academy.lesson01.entity;

/**
 * En esta clase estan generados los enums de los codigos para cada articulo.
 * 
 * @author Emiliano Oliveros
 *
 */
public enum Codigos {

	A1("Chocolate"), A2("Doritos"), A3("Coca"), A4("Gomitas"), A5("Chips"), A6("Jugo"), A7("Galletas"), A8("Canelitas"),
	A9("Halls"), A10("Tarta"), A11("Sabritas"), A12("Cheetos"), A13("Rocaleta"), A14("Rancheritos"), A15("Rufles"),
	A16("Pizza");

	public String codigos;

	private Codigos(String codigos) {
		this.codigos = codigos;
	}

	public String getCodigos() {
		return codigos;
	}

	public void setCodigos(String codigos) {
		this.codigos = codigos;
	}

}