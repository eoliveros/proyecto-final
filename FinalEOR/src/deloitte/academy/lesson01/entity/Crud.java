package deloitte.academy.lesson01.entity;

/**
 * Interfaz con las funciones CRUD(create, read, update, delete)
 * 
 * @author Emiliano Oliveros
 *
 */
public interface Crud {

	/**
	 * Funcion que crea un articulo nuevo.
	 * 
	 * @param articulo
	 */
	public void createProducto(Articulo articulo);

	/**
	 * Funcion que actualiza un articulo.
	 * 
	 * @param articulo
	 * @param cambio
	 */
	public void updateProducto(Articulo articulo, Articulo cambio);

	/**
	 * Funcion que borra un articulo.
	 * 
	 * @param articulo
	 */
	public void deleteProducto(Articulo articulo);

	/**
	 * Funcion que lee un articulo.
	 * 
	 * @param articulo
	 */
	public void readProducto(Articulo articulo);

}
