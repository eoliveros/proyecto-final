package deloitte.academy.lesson01.entity;

/**
 * Clase abstracta que hereda los parametros codigo, articulo, precio y
 * cantidad.
 * 
 * @author Emiliano Oliveros
 *
 */
public abstract class Expendedora {

	/**
	 * Declara las variables propias de cada articulo.
	 */
	public String Codigo;
	public String Articulo;
	public double Precio;
	public int Cantidad;

	public Expendedora() {

	}

	/**
	 * Constructor de los atributos de los articulos.
	 * @param e
	 */
	public Expendedora(Expendedora e) {
		super();
		Codigo = e.Codigo;
		Articulo = e.Articulo;
		Precio = e.Precio;
		Cantidad = e.Cantidad;
	}

	public Expendedora(String codigo, String articulo, double precio, int cantidad) {
		super();
		Codigo = codigo;
		Articulo = articulo;
		Precio = precio;
		Cantidad = cantidad;
	}
	/**
	 * Generacion de getter y setters de las variables del articulo.
	 * @return nothing
	 */

	public String getCodigo() {
		return Codigo;
	}

	public void setCodigo(String codigo) {
		Codigo = codigo;
	}

	public String getArticulo() {
		return Articulo;
	}

	public void setArticulo(String articulo) {
		Articulo = articulo;
	}

	public double getPrecio() {
		return Precio;
	}

	public void setPrecio(double precio) {
		Precio = precio;
	}

	public int getCantidad() {
		return Cantidad;
	}

	public void setCantidad(int cantidad) {
		Cantidad = cantidad;
	}
/**
 * 
 */
	public void vender() {
		// TODO Auto-generated method stub

	}

	@Override
	public abstract String toString();

}
