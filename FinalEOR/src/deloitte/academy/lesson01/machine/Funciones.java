package deloitte.academy.lesson01.machine;

import java.util.List;

import deloitte.academy.lesson01.entity.Articulo;
/**
 * Esta funcion imprime la lista completa de articulos.
 * @author Emiliano Oliveros
 * @since 10/03/2020
 * @version 1.0
 */
public class Funciones {

	public static void imprimeArticulo(List<Articulo> lista) {
		System.out.println("LISTA ARTICULOS");
		for (Articulo aux : lista) {
			aux.readProducto(aux);
			System.out.println("---------------------------------------------");
		}
		System.out.println("---------------------------------------------");

	}

}
