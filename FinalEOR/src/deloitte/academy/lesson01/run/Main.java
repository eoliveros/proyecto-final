package deloitte.academy.lesson01.run;

import java.util.ArrayList;

import deloitte.academy.lesson01.entity.Articulo;
import deloitte.academy.lesson01.entity.Codigos;
import deloitte.academy.lesson01.machine.Funciones;

/**
 * Clase principal que ejecuta las funcionalidades del proyecto.
 * 
 * @author Emiliano Oliveros
 * @since 10/03/2020
 * @version 1.0
 * {@code} public static void main(String[] args) {
 */

public class Main {

	public static final ArrayList<Articulo> listaArticulos2 = new ArrayList<Articulo>();
	public static final ArrayList<Articulo> listaStock = new ArrayList<Articulo>();
	public static final ArrayList<Articulo> listaVendidos = new ArrayList<Articulo>();

	public static void main(String[] args) {
		/*
		 * Relleno de articulos
		 * @throws los sets para generar nuevos articulos.
		 */
		Articulo lista1 = new Articulo();
		lista1.setCodigo("A1");
		lista1.setArticulo(Codigos.A1.getCodigos());
		lista1.setPrecio(10.5);
		lista1.setCantidad(10);

		Articulo lista2 = new Articulo();
		lista2.setCodigo("A2");
		lista2.setArticulo(Codigos.A2.getCodigos());
		lista2.setPrecio(15.5);
		lista2.setCantidad(4);

		Articulo lista3 = new Articulo();
		lista3.setCodigo("A3");
		lista3.setArticulo(Codigos.A3.getCodigos());
		lista3.setPrecio(22.5);
		lista3.setCantidad(2);

		Articulo lista4 = new Articulo();
		lista4.setCodigo("A4");
		lista4.setArticulo(Codigos.A4.getCodigos());
		lista4.setPrecio(8.75);
		lista4.setCantidad(6);

		Articulo lista5 = new Articulo();
		lista5.setCodigo("A5");
		lista5.setArticulo(Codigos.A5.getCodigos());
		lista5.setPrecio(30);
		lista5.setCantidad(10);

		Articulo lista6 = new Articulo();
		lista6.setCodigo("A6");
		lista6.setArticulo(Codigos.A6.getCodigos());
		lista6.setPrecio(15);
		lista6.setCantidad(2);

		Articulo lista7 = new Articulo();
		lista7.setCodigo("B1");
		lista7.setArticulo(Codigos.A7.getCodigos());
		lista7.setPrecio(10);
		lista7.setCantidad(3);

		Articulo lista8 = new Articulo();
		lista8.setCodigo("B2");
		lista8.setArticulo(Codigos.A8.getCodigos());
		lista8.setPrecio(120);
		lista8.setCantidad(6);

		Articulo lista9 = new Articulo();
		lista9.setCodigo("B3");
		lista9.setArticulo(Codigos.A9.getCodigos());
		lista9.setPrecio(10.1);
		lista9.setCantidad(10);

		Articulo lista10 = new Articulo();
		lista10.setCodigo("B4");
		lista10.setArticulo(Codigos.A10.getCodigos());
		lista10.setPrecio(3.14);
		lista10.setCantidad(10);

		Articulo lista11 = new Articulo();
		lista11.setCodigo("B5");
		lista11.setArticulo(Codigos.A11.getCodigos());
		lista11.setPrecio(15.55);
		lista11.setCantidad(0);

		Articulo lista12 = new Articulo();
		lista12.setCodigo("B6");
		lista12.setArticulo(Codigos.A12.getCodigos());
		lista12.setPrecio(12.25);
		lista12.setCantidad(5);

		Articulo lista13 = new Articulo();
		lista13.setCodigo("C1");
		lista13.setArticulo(Codigos.A13.getCodigos());
		lista13.setPrecio(10);
		lista13.setCantidad(1);

		Articulo lista14 = new Articulo();
		lista14.setCodigo("C2");
		lista14.setArticulo(Codigos.A14.getCodigos());
		lista14.setPrecio(14.75);
		lista14.setCantidad(6);

		Articulo lista15 = new Articulo();
		lista15.setCodigo("C3");
		lista15.setArticulo(Codigos.A15.getCodigos());
		lista15.setPrecio(13.15);
		lista15.setCantidad(10);

		Articulo lista16 = new Articulo();
		lista16.setCodigo("C4");
		lista16.setArticulo(Codigos.A16.getCodigos());
		lista16.setPrecio(22);
		lista16.setCantidad(9);

		listaArticulos2.add(lista1);
		listaArticulos2.add(lista2);
		listaArticulos2.add(lista3);
		listaArticulos2.add(lista4);
		listaArticulos2.add(lista5);
		listaArticulos2.add(lista6);
		listaArticulos2.add(lista7);
		listaArticulos2.add(lista8);
		listaArticulos2.add(lista9);
		listaArticulos2.add(lista10);
		listaArticulos2.add(lista11);
		listaArticulos2.add(lista12);
		listaArticulos2.add(lista13);
		listaArticulos2.add(lista14);
		listaArticulos2.add(lista15);
		listaArticulos2.add(lista16);

		/**
		 * Llamada a funciones
		 * @see crud
		 */
		lista1.vender();
		Funciones.imprimeArticulo(listaArticulos2);
		lista1.createProducto(lista1);
		lista1.readProducto(lista16);
		lista1.updateProducto(lista15, lista1);
		Funciones.imprimeArticulo(listaArticulos2);
		lista1.deleteProducto(lista2);
		lista1.vender();

	}
}
